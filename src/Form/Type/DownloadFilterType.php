<?php

declare(strict_types=1);

/**
 * Created by fensterart.de c413.
 * Developer: Sven Rhinow (sven@sr-tag.de)
 * Date: 23.01.24
 */

namespace Srhinow\DownloadBundle\Form\Type;

use Contao\CoreBundle\InsertTag\InsertTagParser;
use Contao\ModuleModel;
use Srhinow\DownloadBundle\Helper\TagHelper;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\OptionsResolver\OptionsResolver;

class DownloadFilterType extends AbstractType
{
    protected InsertTagParser $insertTagParser;
    protected RequestStack $requestStack;

    public function __construct(InsertTagParser $insertTagParser, RequestStack $requestStack)
    {
        $this->insertTagParser = $insertTagParser;
        $this->requestStack = $requestStack;
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        /** @var ModuleModel $model */
        $model = $options['fmd'];
        $builder->add('REQUEST_TOKEN', ContaoRequestTokenType::class);

        $builder->add('tag', ChoiceType::class, [
            'choices' => array_flip(TagHelper::getTagOptions()),
            'multiple' => false,
            'expanded' => true,
            'label' => false,
            'required' => false,
            'row_attr' => [
                'class' => 'widget-select',
            ],
            'data' => $this->requestStack->getMainRequest()->get('tag', ''),
        ]);

        $builder->add('search', TextType::class, [
            'required' => false,
            'attr' => [
                'class' => 'filter_field text',
            ],
            'data' => $this->requestStack->getMainRequest()->get('search', '')
        ]);
        $builder->add('submit', SubmitType::class);

    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'fmd' => null
        ]);
    }

    public function getHeadlineHtml(?string $content, string $type): string
    {
        if (empty($content)) {
            return '';
        }

        $return = '<div class="download_filter_widget_headline '.$type.'">';
        $return .= $this->insertTagParser->replace($content);
        $return .= '</div>';

        return $return;
    }

    public function getBlockPrefix()
    {
        return '';
    }
}
