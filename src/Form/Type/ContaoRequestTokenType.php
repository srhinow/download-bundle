<?php

declare(strict_types=1);


namespace Srhinow\DownloadBundle\Form\Type;

use Srhinow\DownloadBundle\Csrf\DownloadCsrfTokenManager;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ContaoRequestTokenType extends AbstractType
{
    private DownloadCsrfTokenManager $csrfTokenManager;

    public function __construct(
        DownloadCsrfTokenManager $csrfTokenManager
    ) {
        $this->csrfTokenManager = $csrfTokenManager;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data' => $this->csrfTokenManager->generateToken(),
            'empty_data' => $this->csrfTokenManager->generateToken(),
            'mapped' => false,
        ]);
    }

    public function getParent(): string
    {
        return HiddenType::class;
    }

    public function buildView(FormView $view, FormInterface $form, array $options): void
    {
        parent::buildView($view, $form, $options);

        $view->vars['full_name'] = 'REQUEST_TOKEN';
    }
}
