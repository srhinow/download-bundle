<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension download-bundle.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license LGPL 3.0 or later
 */

$GLOBALS['BE_SRHINOW_DOWNLOADS']['PROPERTIES']['PUBLICSRC'] = 'bundles/srhinowdownload';

/*
 * -------------------------------------------------------------------------
 * Back end modules
 * -------------------------------------------------------------------------
 */
\Contao\ArrayUtil::arrayInsert($GLOBALS['BE_MOD']['content'], 1, [
    'downloads' => [
        'tables' => ['tl_downloads', 'tl_download_categories'],
        'icon' => $GLOBALS['BE_SRHINOW_DOWNLOADS']['PROPERTIES']['PUBLICSRC'].'/icons/download.png',
    ],
]);


/*
 * -------------------------------------------------------------------------
 * CONTENT ELEMENT
 * -------------------------------------------------------------------------
 */
\Contao\ArrayUtil::arrayInsert($GLOBALS['TL_CTE'], 1, ['simple_downloads' => []]);
$GLOBALS['TL_CTE']['simple_downloads']['downloadList'] = 'Srhinow\DownloadBundle\Elements\ContentDownloadList';

/*
 * -------------------------------------------------------------------------
 * Models
 * -------------------------------------------------------------------------
 */
$GLOBALS['TL_MODELS']['tl_downloads'] = \Srhinow\DownloadBundle\Models\DownloadsModel::class;
$GLOBALS['TL_MODELS']['tl_download_categories'] = \Srhinow\DownloadBundle\Models\DownloadCategoriesModel::class;

/*
 * -------------------------------------------------------------------------
 * HOOKS
 * -------------------------------------------------------------------------
 */
$GLOBALS['TL_HOOKS']['getSearchablePages'][] = [
    'srhinow.download_bundle.listener.hook.get_searchable_pages',
    'onGetSearchablePages',
];
