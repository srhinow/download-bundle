<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension download-bundle.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license LGPL 3.0 or later
 */

/*
 * Table tl_downloads.
 */
$GLOBALS['TL_DCA']['tl_downloads'] = [
    // Config
    'config' => [
        'dataContainer' => 'Table',
        'enableVersioning' => true,
        'sql' => [
            'keys' => [
                'id' => 'primary',
            ],
        ],
    ],
    // List
    'list' => [
        'sorting' => [
            'mode' => SORT_STRING,
            'fields' => ['name'],
            'panelLayout' => 'filter;sort,search,limit',
        ],
        'label' => [
            'fields' => ['file', 'name','category', 'tags'],
            'showColumns'             => true,
            'label_callback' => ['srhinow.download_bundle.listener.dca.downloads', 'listEntries'],
        ],
        'global_operations' => [
            'categories' => [
                'label' => &$GLOBALS['TL_LANG']['tl_downloads']['categories'],
                'href' => 'table=tl_download_categories',
                'class' => 'header_icon',
                'icon' => $GLOBALS['BE_SRHINOW_DOWNLOADS']['PROPERTIES']['PUBLICSRC'].'/icons/categories.png',
                'attributes' => 'onclick="Backend.getScrollOffset();"',
            ],
            'all' => [
                'label' => &$GLOBALS['TL_LANG']['MSC']['all'],
                'href' => 'act=select',
                'class' => 'header_edit_all',
                'attributes' => 'onclick="Backend.getScrollOffset();"',
            ],
        ],
        'operations' => [
            'edit' => [
                'label' => &$GLOBALS['TL_LANG']['tl_downloads']['edit'],
                'href' => 'act=edit',
                'icon' => 'edit.gif',
            ],
            'copy' => [
                'label' => &$GLOBALS['TL_LANG']['tl_downloads']['copy'],
                'href' => 'act=copy',
                'icon' => 'copy.gif',
            ],
            'delete' => [
                'label' => &$GLOBALS['TL_LANG']['tl_downloads']['delete'],
                'href' => 'act=delete',
                'icon' => 'delete.gif',
                'attributes'          => 'onclick="if(!confirm(\'' . ($GLOBALS['TL_LANG']['MSC']['deleteConfirm'] ?? null) . '\'))return false;Backend.getScrollOffset()"',
            ],
            'toggle' => [
                'label' => &$GLOBALS['TL_LANG']['tl_downloads']['toggle'],
                'icon' => 'visible.gif',
                'attributes' => 'onclick="Backend.getScrollOffset();return AjaxRequest.toggleVisibility(this,%s)"',
                'button_callback' => ['srhinow.download_bundle.listener.dca.downloads', 'toggleIcon'],
            ],
            'show' => [
                'label' => &$GLOBALS['TL_LANG']['tl_downloads']['show'],
                'href' => 'act=show',
                'icon' => 'show.gif',
            ],
        ],
    ],
    // Palettes
    'palettes' => [
        '__selector__'                => array('type', 'addImage'),
        'default' => '
            {default_legend},name,alias,category,file,text;
            {meta_legend},pageTitle,description;
            {image_legend},addImage;
            {tag_legend},tags;
            {extend_legend},published,direct_link,sorting',
    ],
    // Subpalettes
    'subpalettes' => [
        'addImage'                    => 'singleSRC',
    ],
    // Fields
    'fields' => [
        'id' => [
            'sql' => 'int(10) unsigned NOT NULL auto_increment',
        ],
        'tstamp' => [
            'sql' => "int(10) unsigned NOT NULL default '0'",
        ],
        'modify' => [
            'sql' => "int(10) unsigned NOT NULL default '0'",
        ],
        'sorting' => [
            'inputType' => 'text',
            'sorting' => true,
            'eval' => ['tl_class' => 'clr w50'],
            'sql' => "int(10) unsigned NOT NULL default '0'",
        ],
        'name' => [
            'label' => &$GLOBALS['TL_LANG']['tl_downloads']['name'],
            'exclude' => true,
            'search' => true,
            'sorting' => true,
            'inputType' => 'text',
            'eval' => [
                'mandatory' => true,
                'maxlength' => 255,
                'decodeEntities' => true,
                'tl_class' => 'w50',
            ],
            'sql' => "varchar(255) NOT NULL default ''",
        ],
        'alias' => [
            'label' => &$GLOBALS['TL_LANG']['tl_downloads']['alias'],
            'exclude' => true,
            'inputType' => 'text',
            'eval' => [
                'rgxp' => 'alnum',
                'doNotCopy' => true,
                'spaceToUnderscore' => true,
                'maxlength' => 128,
                'tl_class' => 'w50',
            ],
            'save_callback' => [
                ['srhinow.download_bundle.listener.dca.downloads', 'generateAlias'],
            ],
            'sql' => "varchar(128) NOT NULL default ''",
        ],
        'category' => [
            'exclude' => true,
            'filter' => true,
            'inputType' => 'select',
            'foreignKey' => 'tl_download_categories.name',
            'eval' => ['mandatory' => true, 'chosen' => true, 'tl_class' => 'm12 w50', 'multiple' => true],
            'sql' => 'blob NULL',
            'options_callback' => [
                'srhinow.download_bundle.listener.dca.downloads', 'getDownloadCategoryOptions'
            ]
        ],
        'file' => [
            'exclude' => true,
            'inputType' => 'fileTree',
            'eval' => ['filesOnly' => true, 'fieldType' => 'radio', 'mandatory' => true, 'tl_class' => 'clr'],
            'sql' => 'binary(16) NULL',
        ],
        'dl_url' => [
            'inputType' => 'text',
            'sql' => "varchar(255) NOT NULL default ''",
        ],
        'tags' => [
            'label' => &$GLOBALS['TL_LANG']['MSC']['tags'],
            'inputType' => 'tag',
            'sorting' => true,
            'eval' => ['tl_class' => 'clr long'],
            'sql' => "char(1) NOT NULL default ''",
        ],
        'text' => [
            'exclude' => true,
            'search' => true,
            'inputType' => 'textarea',
            'eval' => ['rte' => 'tinyMCE', 'tl_class' => 'clr'],
            'sql' => 'text NULL',
        ],
        'direct_link' => [
            'label' => &$GLOBALS['TL_LANG']['tl_downloads']['direct_link'],
            'filter' => true,
            'sorting' => true,
            'inputType' => 'checkbox',
            'flag' => 11,
            'sql' => "char(1) NOT NULL default ''",
        ],
        'addImage' => array
        (
            'exclude'                 => true,
            'inputType'               => 'checkbox',
            'eval'                    => array('submitOnChange'=>true),
            'sql'                     => "char(1) COLLATE ascii_bin NOT NULL default ''"
        ),
        'singleSRC' => array
        (
            'exclude'                 => true,
            'inputType'               => 'fileTree',
            'eval'                    => array(
                'filesOnly'=>true,
                'fieldType'=>'radio',
                'mandatory'=>true,
                'tl_class'=>'clr',
                'extensions' => '%contao.image.valid_extensions%',
            ),
            'sql'                     => "binary(16) NULL"
        ),
        'published' => [
            'label' => &$GLOBALS['TL_LANG']['tl_downloads']['published'],
            'filter' => true,
            'sorting' => true,
            'inputType' => 'checkbox',
            'flag' => 11,
            'sql' => "char(1) NOT NULL default ''",
        ],
        'pageTitle' => [
            'exclude' => true,
            'search' => true,
            'inputType' => 'text',
            'eval' => ['maxlength' => 255, 'decodeEntities' => true, 'tl_class' => 'w50'],
            'sql' => "varchar(255) NOT NULL default ''",
        ],
        'description' => [
            'label' => &$GLOBALS['TL_LANG']['tl_downloads']['meta_description'],
            'exclude' => true,
            'search' => true,
            'inputType' => 'textarea',
            'eval' => ['decodeEntities' => true, 'tl_class' => 'clr long'],
            'sql' => 'text NULL',
        ],
    ],
];
