<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension download-bundle.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license LGPL 3.0 or later
 */

/*
 * Table tl_download_categories.
 */
$GLOBALS['TL_DCA']['tl_download_categories'] = [
    // Config
    'config' => [
        'dataContainer' => 'Table',
        'enableVersioning' => true,
        'label' => &$GLOBALS['TL_LANG']['tl_download_categories']['title'],
        'sql' => [
            'keys' => [
                'id' => 'primary',
                'pid' => 'index',
            ],
        ],
        'backlink' => 'do=downloads',
    ],
    // List
    'list' => [
        'sorting' => array
        (
            'mode'                    => 5,
            'fields'                  => array('name', 'alias'),
            'flag'                    => 1,
            'panelLayout'             => 'search,sort,filter,limit ',
            'icon'                    => $GLOBALS['BE_SRHINOW_DOWNLOADS']['PROPERTIES']['PUBLICSRC'].'/icons/icon.gif',
        ),
        'label' => array
        (
            'fields'                  => array('name'),
            'format'                  => '%s'
        ),
        'global_operations' => [
            'all' => [
                'label' => &$GLOBALS['TL_LANG']['MSC']['all'],
                'href' => 'act=select',
                'class' => 'header_edit_all',
                'attributes' => 'onclick="Backend.getScrollOffset();"',
            ],
        ],
        'operations' => [
            'edit' => [
                'label' => &$GLOBALS['TL_LANG']['tl_download_categories']['edit'],
                'href' => 'act=edit',
                'icon' => 'edit.gif',
            ],
            'copy' => [
                'label' => &$GLOBALS['TL_LANG']['tl_download_categories']['copy'],
                'href' => 'act=copy',
                'icon' => 'copy.gif',
            ],
            'copyChildren' => array
            (
                'label'               => &$GLOBALS['TL_LANG']['tl_download_categories']['copyChildren'],
                'href'                => 'act=paste&amp;mode=copy&amp;childs=1',
                'icon'                => 'copychilds.gif',
                'attributes'          => 'onclick="Backend.getScrollOffset();"',
            ),
            'delete' => [
                'label' => &$GLOBALS['TL_LANG']['tl_download_categories']['delete'],
                'href' => 'act=delete',
                'icon' => 'delete.gif',
                'attributes'          => 'onclick="if(!confirm(\'' . ($GLOBALS['TL_LANG']['MSC']['deleteConfirm'] ?? null) . '\'))return false;Backend.getScrollOffset()"',
            ],
            'cut' => array
            (
                'label'               => &$GLOBALS['TL_LANG']['tl_download_categories']['cut'],
                'href'                => 'act=paste&amp;mode=cut',
                'icon'                => 'cut.gif',
                'attributes'          => 'onclick="Backend.getScrollOffset();"',
            ),
            'show' => [
                'label' => &$GLOBALS['TL_LANG']['tl_download_categories']['show'],
                'href' => 'act=show',
                'icon' => 'show.gif',
            ],
        ],
    ],

    // Palettes
    'palettes' => [
        'default' => '{title_legend},name,alias,sorting',
    ],

    // Fields
    'fields' => [
        'id' => [
            'sql' => 'int(10) unsigned NOT NULL auto_increment',
        ],
        'pid' => [
            'sql' => "int(10) unsigned NOT NULL default '0'",
        ],
        'sorting' => [
            'exclude' => true,
            'inputType' => 'text',
            'eval' => ['tl_class' => 'w50'],
            'sql' => "int(10) unsigned NOT NULL default '0'",
        ],
        'tstamp' => [
            'sql' => "int(10) unsigned NOT NULL default '0'",
        ],
        'modify' => [
            'sql' => "int(10) unsigned NOT NULL default '0'",
        ],
        'name' => [
            'label' => &$GLOBALS['TL_LANG']['tl_download_categories']['name'],
            'exclude' => true,
            'inputType' => 'text',
            'eval' => ['mandatory' => true, 'maxlength' => 255, 'tl_class' => 'w50'],
            'sql' => "varchar(255) NOT NULL default ''",
        ],

        'alias' => [
            'label' => &$GLOBALS['TL_LANG']['tl_download_categories']['alias'],
            'exclude' => true,
            'inputType' => 'text',
            'eval' => ['maxlength' => 255, 'tl_class' => 'w50'],
            'save_callback' => [
                ['srhinow.download_bundle.listener.dca.download_categories', 'generateAlias'],
            ],
            'sql' => "varchar(128) NOT NULL default ''",
        ],
    ],
];
