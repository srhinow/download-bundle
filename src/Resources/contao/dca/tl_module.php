<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension download-bundle.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license LGPL 3.0 or later
 */

/*
 * Add palettes to tl_module.
 */
$GLOBALS['TL_DCA']['tl_module']['palettes']['download_filter'] = '{title_legend},name,type;{download_legend},jumpTo;{template_legend:hide},download_template,customTpl';
$GLOBALS['TL_DCA']['tl_module']['palettes']['download_list'] = '{title_legend},name,headline,type;{download_legend},jumpTo,download_size_decimal;{template_legend:hide},download_template,customTpl;{protected_legend:hide},protected;{expert_legend:hide},cssID,space';
$GLOBALS['TL_DCA']['tl_module']['palettes']['download_search'] = '{title_legend},name,headline,type;{template_legend:hide},download_template,customTpl;{protected_legend:hide},protected;{expert_legend:hide},cssID,space';
$GLOBALS['TL_DCA']['tl_module']['palettes']['download_detail'] = '{title_legend},name,headline,type;{template_legend:hide},download_template,customTpl;{protected_legend:hide},protected;{expert_legend:hide},cssID,space';

/*
 * Add fields to tl_module
 */
$GLOBALS['TL_DCA']['tl_module']['fields']['download_template'] = [
    'default' => 'download_simple',
    'exclude' => true,
    'inputType' => 'select',
    'options_callback' => ['srhinow.download_bundle.listener.dca.module', 'getDownloadTemplates'],
    'eval' => ['tl_class' => 'w50'],
    'sql' => "varchar(32) NOT NULL default ''",
];

$GLOBALS['TL_DCA']['tl_module']['fields']['download_size_decimal'] = [
    'inputType' => 'text',
    'default' => 1,
    'sql' => "int(1) NOT NULL default 1",
];