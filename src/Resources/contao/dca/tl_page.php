<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension download-bundle.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license LGPL 3.0 or later
 */
use Contao\CoreBundle\DataContainer\PaletteManipulator;

$GLOBALS['TL_DCA']['tl_page']['fields']['download_categories'] = [
    'exclude' => true,
    'filter' => true,
    'inputType' => 'select',
    'foreignKey' => 'tl_download_categories.name',
    'eval' => ['chosen' => true, 'tl_class' => 'm12', 'multiple' => true],
    'options_callback' => ['srhinow.download_bundle.listener.dca.module', 'getCategoryOptions'],
    'sql' => 'blob NULL',
];
$GLOBALS['TL_DCA']['tl_page']['fields']['only_subcategories'] = [
    'exclude' => true,
    'filter' => false,
    'inputType' => 'checkbox',
    'sql' => "char(1) NOT NULL default ''",
];
$GLOBALS['TL_DCA']['tl_page']['fields']['download_tags'] = [
    'exclude' => true,
    'filter' => true,
    'inputType' => 'select',
    'foreignKey' => 'tl_tag.tag',
    'eval' => ['chosen' => true, 'tl_class' => 'm12', 'multiple' => true],
    'options_callback' => ['srhinow.download_bundle.listener.dca.module', 'getTagOptions'],
    'sql' => 'blob NULL',
];
PaletteManipulator::create()
    // add a new "custom_legend" before the "date_legend"
    ->addLegend('download_module_legend', 'expert_legend', PaletteManipulator::POSITION_AFTER)

    // directly add new fields to the new legend
    ->addField('download_categories', 'download_module_legend', PaletteManipulator::POSITION_APPEND)
    ->addField('only_subcategories', 'download_module_legend', PaletteManipulator::POSITION_APPEND)
    ->addField('download_tags', 'download_module_legend', PaletteManipulator::POSITION_APPEND)

    // then apply it to the palette as usual
    ->applyToPalette('regular', 'tl_page')
;
