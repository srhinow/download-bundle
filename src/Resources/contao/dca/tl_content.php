<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension download-bundle.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license LGPL 3.0 or later
 */

/*
 * Add palettes to tl_content.
 */
$GLOBALS['TL_DCA']['tl_content']['palettes']['__selector__'][] = 'do_category_filter';
$GLOBALS['TL_DCA']['tl_content']['palettes']['downloadList'] = '
    {title_legend},name,headline,type;
    {download_legend},jumpTo;
    {filter_legend},do_category_filter;
    {template_legend:hide},download_template,customTpl;
    {protected_legend:hide},protected;
    {expert_legend:hide},cssID,space,guests
    ';
$GLOBALS['TL_DCA']['tl_content']['subpalettes']['do_category_filter'] = 'download_categories';
/*
 * Add fields to tl_content
 */
$GLOBALS['TL_DCA']['tl_content']['fields']['download_template'] = [
    'label' => &$GLOBALS['TL_LANG']['tl_content']['download_template'],
    'default' => 'download_simple',
    'exclude' => true,
    'inputType' => 'select',
    'options_callback' => ['srhinow.download_bundle.listener.dca.module', 'getDownloadTemplates'],
    'eval' => ['tl_class' => 'w50'],
    'sql' => "varchar(32) NOT NULL default ''",
];

// Add fields to tl_content
$GLOBALS['TL_DCA']['tl_content']['fields']['download_categories'] = [
    'label' => &$GLOBALS['TL_LANG']['tl_content']['download_categories'],
    'exclude' => true,
    'inputType' => 'checkbox',
    'options_callback' => ['srhinow.download_bundle.listener.dca.module', 'getCategoryOptions'],
    'eval' => ['mandatory' => true, 'multiple' => true],
    'sql' => 'blob NULL',
];
$GLOBALS['TL_DCA']['tl_content']['fields']['do_category_filter'] = [
    'label' => &$GLOBALS['TL_LANG']['tl_content']['do_category_filter'],
    'exclude' => true,
    'filter' => false,
    'inputType' => 'checkbox',
    'eval' => ['submitOnChange' => true],
    'sql' => "char(1) NOT NULL default ''",
];
$GLOBALS['TL_DCA']['tl_content']['fields']['jumpTo'] = [
    'exclude' => true,
    'inputType' => 'pageTree',
    'foreignKey' => 'tl_page.title',
    'eval' => ['mandatory' => true, 'fieldType' => 'radio', 'tl_class' => 'clr'],
    'sql' => 'int(10) unsigned NOT NULL default 0',
    'relation' => ['type' => 'hasOne', 'load' => 'lazy'],
];
