<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension download-bundle.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license LGPL 3.0 or later
 */

namespace Srhinow\DownloadBundle\Elements;

use Contao\BackendTemplate;
use Contao\Controller;
use Contao\Input;
use Srhinow\DownloadBundle\Models\DownloadCategoriesModel;
use Srhinow\DownloadBundle\Models\DownloadsModel;
use Srhinow\DownloadBundle\Modules\ModuleDownloads;

class ContentDownloadList extends ModuleDownloads
{
    /**
     * Template.
     *
     * @var string
     */
    protected $strTemplate = 'ce_download_list';

    /**
     * Display a wildcard in the back end.
     *
     * @return string
     */
    public function generate()
    {
        if (TL_MODE === 'BE') {
            /** @var \BackendTemplate|object $objTemplate */
            $objTemplate = new BackendTemplate('be_wildcard');

            $objTemplate->wildcard = '### '.mb_strtoupper($GLOBALS['TL_LANG']['CTE']['downloadList'][0]).' ###';
            $objTemplate->title = $this->headline;
            $objTemplate->id = $this->id;
            $objTemplate->link = $this->name;
            $objTemplate->href = 'contao/main.php?do=themes&amp;table=tl_module&amp;act=edit&amp;id='.$this->id;

            return $objTemplate->parse();
        }

        $file = Input::get('file');

        // Send the file to the browser and do not send a 404 header (see #4632)
        if (null !== $file) {
            Controller::sendFileToBrowser($file);
        }

        return parent::generate();
    }

    /**
     * @return array
     */
    public function findDownloadsByCustomCategories(array $catFilter)
    {
        $categories = [];

        if (!\is_array($catFilter) || \count($catFilter) < 1) {
            return $categories;
        }

        $dwlCategoriesObj = DownloadCategoriesModel::findMultipleByIds($catFilter, ['order' => 'sorting ASC']);

        if (null !== $dwlCategoriesObj) {
            while ($dwlCategoriesObj->next()) {
                if (DownloadsModel::countPublishedByCategory($dwlCategoriesObj->id) < 1) {
                    continue;
                }

                $objDownloads = DownloadsModel::findPublishedByCategory($dwlCategoriesObj->id);

                $entries = (null !== $objDownloads) ? $this->parseDownloads($objDownloads) : [];

                $categories[] = [
                    'id' => $dwlCategoriesObj->id,
                    'name' => $dwlCategoriesObj->name,
                    'alias' => $dwlCategoriesObj->alias,
                    'entries' => $entries,
                ];
            }
        }

        return $categories;
    }

    /**
     * Generate the module.
     */
    protected function compile(): void
    {
        $this->Template->categories = $categories = [];
        $this->Template->empty = $GLOBALS['TL_LANG']['MSC']['emptyDownloadList'];

        if ((int) $this->do_category_filter > 0 && $arrCats = unserialize($this->download_categories)) {
            $this->Template->categories = $this->findDownloadsByCustomCategories($arrCats);
        } else {
            $this->Template->categories = $this->findAllDownloadsByAllCategories();
        }
    }

    /**
     * @return array
     */
    protected function findAllDownloadsByAllCategories()
    {
        $categories = [];
        $dwlCategoriesObj = DownloadCategoriesModel::findAll(['order' => 'sorting ASC']);

        if (null !== $dwlCategoriesObj) {
            while ($dwlCategoriesObj->next()) {
                if (DownloadsModel::countPublishedByCategory($dwlCategoriesObj->id) < 1) {
                    continue;
                }

                $objDownloads = DownloadsModel::findPublishedByCategory($dwlCategoriesObj->id);

                $entries = (null !== $objDownloads) ? $this->parseDownloads($objDownloads) : [];

                $categories[] = [
                    'id' => $dwlCategoriesObj->id,
                    'name' => $dwlCategoriesObj->name,
                    'alias' => $dwlCategoriesObj->alias,
                    'entries' => $entries,
                ];
            }
        }

        return $categories;
    }
}
