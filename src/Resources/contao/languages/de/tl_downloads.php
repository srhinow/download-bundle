<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension download-bundle.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license LGPL 3.0 or later
 */

$GLOBALS['TL_LANG']['tl_downloads']['title'] = 'Downloads';
$GLOBALS['TL_LANG']['tl_downloads']['categories'] = 'Download Kategorien';

/* Legends */
$GLOBALS['TL_LANG']['tl_downloads']['default_legend'] = 'Haupt-Einstellungen';
$GLOBALS['TL_LANG']['tl_downloads']['meta_legend'] = 'Meta-Einstellungen';
$GLOBALS['TL_LANG']['tl_downloads']['tag_legend'] = 'Tag-Einstellungen';
$GLOBALS['TL_LANG']['tl_downloads']['extend_legend'] = 'weitere Einstellungen';
$GLOBALS['TL_LANG']['tl_downloads']['image_legend'] = 'Bild Einstellungen';

/* Fields */
$GLOBALS['TL_LANG']['tl_downloads']['name'] = [
    'Download-Name',
    'Name des Downloads.',
];
$GLOBALS['TL_LANG']['tl_downloads']['alias'] = [
    'Alias',
    'Alias, um auf den Download verweisen zu können.',
];
$GLOBALS['TL_LANG']['tl_downloads']['sorting'] = [
    'Sortierung',
    'Hier nur Zahlen eingeben. Nach dieser wird auf dar Website aufsteigend sortiert.',
];
$GLOBALS['TL_LANG']['tl_downloads']['category'] = [
    'Kategorien',
    'die zugehoerigen Kategorien auswählen.',
];
$GLOBALS['TL_LANG']['tl_downloads']['file'] = [
    'Datei',
    'Datei auswählen, die zum Dowenload bereit gestellt weden soll.',
];
$GLOBALS['TL_LANG']['tl_downloads']['addImage'] = [
    'Bild',
    'Bild zum Download anlegen.',
];
$GLOBALS['TL_LANG']['tl_downloads']['singleSRC'] = [
    'Bild zum Download auswählen',
    'Ein Bild zum Download was z.B. eine PDF-Vorschau sein kann.',
];
$GLOBALS['TL_LANG']['tl_downloads']['published'] = [
    'online sichtbar',
    'Haken setzen wenn dieser Eintrag auf der Website angezeigt werden soll.',
];
$GLOBALS['TL_LANG']['tl_downloads']['direct_link'] = [
    'Direkt-Link',
    'Den Eintrag in der Listenansicht direkt auf die Datei und nicht auf die Detailseite verlinken.',
];
$GLOBALS['TL_LANG']['tl_downloads']['pageTitle'] = [
    'Seiten-Titel',
    'Wenn hier etwas drin steht wird der Standart-Seitentitel neu gesetzt.',
];
$GLOBALS['TL_LANG']['tl_downloads']['description'] = [
    'Meta-Beschreibung',
    'optionale Beschreibung zur Datei.',
];
$GLOBALS['TL_LANG']['tl_downloads']['text'] = ['Download-Beschreibung'];

/* Activities */
$GLOBALS['TL_LANG']['tl_downloads']['new']['0'] = 'Neuer Download';
$GLOBALS['TL_LANG']['tl_downloads']['new']['1'] = 'Neuen Download erstellen.';
$GLOBALS['TL_LANG']['tl_downloads']['show']['0'] = 'Einzelheiten vom Download';
$GLOBALS['TL_LANG']['tl_downloads']['show']['1'] = 'Einzelheiten vom Download ID %s anzeigen';
$GLOBALS['TL_LANG']['tl_downloads']['edit']['0'] = 'Download bearbeiten';
$GLOBALS['TL_LANG']['tl_downloads']['edit']['1'] = 'Download ID %s bearbeiten';
$GLOBALS['TL_LANG']['tl_downloads']['copy']['0'] = 'Download kopieren';
$GLOBALS['TL_LANG']['tl_downloads']['copy']['1'] = 'Download ID %s kopieren';
$GLOBALS['TL_LANG']['tl_downloads']['cut']['0'] = 'Download verschieben';
$GLOBALS['TL_LANG']['tl_downloads']['cut']['1'] = 'Download ID %s verschieben';
$GLOBALS['TL_LANG']['tl_downloads']['delete']['0'] = 'Download löschen';
$GLOBALS['TL_LANG']['tl_downloads']['delete']['1'] = 'Download ID %s löschen';
$GLOBALS['TL_LANG']['tl_downloads']['toggle']['0'] = 'aktivieren/ deaktivieren';
$GLOBALS['TL_LANG']['tl_downloads']['toggle']['1'] = 'Den Eintrag aktivieren bzw. deaktivieren';
