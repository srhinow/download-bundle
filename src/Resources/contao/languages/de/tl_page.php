<?php

// Legends
$GLOBALS['TL_LANG']['tl_page']['download_module_legend'] = 'Download-Modul - Einstellungen';

// Fields
$GLOBALS['TL_LANG']['tl_page']['download_categories'] = [
    'Download-Kategorien',
    'Die auf dieser Seite eingebundenen Module können sich auf diese Download-Categorie-Auswahl beziehen.',
];
$GLOBALS['TL_LANG']['tl_page']['only_subcategories'] = [
    'nur Unter-Kategorien anzeigen',
    'Nur Downloads Unter-Kategorie/n werden von den ausgewählten Kategorien angezeigt.',
];
$GLOBALS['TL_LANG']['tl_page']['download_tags'] = [
    'Download-Tags',
    'Die auf dieser Seite eingebundenen Module, beziehen sich auf diese Download-Tags-Auswahl. Wenn nichts eingetragen wird, werden alle Tags z.B. beim Filter angezeigt.',
];
