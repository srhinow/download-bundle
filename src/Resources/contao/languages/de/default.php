<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension download-bundle.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license LGPL 3.0 or later
 */

$GLOBALS['TL_LANG']['MSC']['emptyDownloadList'] = 'Zurzeit sind keine Downloads vorhanden.';
$GLOBALS['TL_LANG']['MSC']['noSearchAndNoTags'] = 'Bitte wählen sie ein Schlagwort aus oder geben sie ein Suchbegriff ein.';
$GLOBALS['TL_LANG']['MSC']['noEntriesBySearch'] = 'Für die Filter-Eigenschaften sind keine Ergebnisse vorhanden.';
