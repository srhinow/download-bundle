<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension download-bundle.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license LGPL 3.0 or later
 */

$GLOBALS['TL_LANG']['tl_content']['download_legend'] = 'Download - Einstellungen';
$GLOBALS['TL_LANG']['tl_content']['filter_legend'] = 'Filter - Einstellungen';

/*
 * Fields
 */
$GLOBALS['TL_LANG']['tl_content']['download_template'] = [
    'Download-Template',
    'Wird zur darstellung des einzelnen Elements benötigt.',
];
$GLOBALS['TL_LANG']['tl_content']['do_category_filter'] = [
    'nach Kategorien filtern',
    'Soll es die Möglichkeit geben nur Download bestimmter Kategorien anzuzeigen statt alle dann 
    aktivieren sie die Checkbox.',
];
$GLOBALS['TL_LANG']['tl_content']['download_categories'] = [
    'Kategorien',
    'Nur Downloads dieser Kategorie/n werden angezeigt.',
];
$GLOBALS['TL_LANG']['tl_content']['jumpTo'] = [
    'Detailseite',
    'Geben sie hier an auf welcher Seite das Download-Detail-Modul eingebunden ist.',
];
