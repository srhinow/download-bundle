<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension download-bundle.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license LGPL 3.0 or later
 */

/*
 * Legends.
 */
$GLOBALS['TL_LANG']['tl_download_categories']['title_legend'] = 'Einstellungen';

/*
 * Fields
 */
$GLOBALS['TL_LANG']['tl_download_categories']['title'] = 'Download Kategorien';
$GLOBALS['TL_LANG']['tl_download_categories']['name']['0'] = 'Name';
$GLOBALS['TL_LANG']['tl_download_categories']['name']['1'] = 'Name der Kategorie.';
$GLOBALS['TL_LANG']['tl_download_categories']['alias']['0'] = 'Alias';
$GLOBALS['TL_LANG']['tl_download_categories']['alias']['1'] = 'Alias, um auf die Kategorie verweisen zu können.';
$GLOBALS['TL_LANG']['tl_download_categories']['new']['0'] = 'Neue Kategorie';
$GLOBALS['TL_LANG']['tl_download_categories']['new']['1'] = 'Neue Kategorie erstellen.';
$GLOBALS['TL_LANG']['tl_download_categories']['show']['0'] = 'Einzelheiten der Kategorie';
$GLOBALS['TL_LANG']['tl_download_categories']['show']['1'] = 'Einzelheiten der Kategorie ID %s anzeigen';
$GLOBALS['TL_LANG']['tl_download_categories']['edit']['0'] = 'Kategorie bearbeiten';
$GLOBALS['TL_LANG']['tl_download_categories']['edit']['1'] = 'Kategorie ID %s bearbeiten';
$GLOBALS['TL_LANG']['tl_download_categories']['copy']['0'] = 'Kategorie kopieren';
$GLOBALS['TL_LANG']['tl_download_categories']['copy']['1'] = 'Kategorie ID %s kopieren';
$GLOBALS['TL_LANG']['tl_download_categories']['cut']['0'] = 'Begriff verschieben';
$GLOBALS['TL_LANG']['tl_download_categories']['cut']['1'] = 'Begriff ID %s verschieben';
$GLOBALS['TL_LANG']['tl_download_categories']['delete']['0'] = 'Kategorie löschen';
$GLOBALS['TL_LANG']['tl_download_categories']['delete']['1'] = 'Kategorie ID %s löschen';
$GLOBALS['TL_LANG']['tl_download_categories']['copyChildren']['0'] = 'Begriff mit Unterbegriffen kopieren';
$GLOBALS['TL_LANG']['tl_download_categories']['copyChildren']['1'] = 'Begriff ID %s mit Unterbegriffen kopieren';
