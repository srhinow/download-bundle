<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension download-bundle.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license LGPL 3.0 or later
 */

/*
 * Content elements.
 */
$GLOBALS['TL_LANG']['CTE']['downloads'] = [
    'Downloads',
    'Dieses Modul verwaltet kategorisierbare Downloads für Frontendmodul-Ausgaben.',
];
$GLOBALS['TL_LANG']['CTE']['downloadList'] = ['Download-Liste'];

/*
 * Back end modules.
 */
$GLOBALS['TL_LANG']['MOD']['downloads'] = [
    'Downloads',
    'Dieses Modul verwaltet kategorisierbare Downloads für Frontendmodul-Ausgaben.',
];

/*
 * Frontend modules
 */
$GLOBALS['TL_LANG']['FMD']['downloads'] = ['einfache Downloads'];
$GLOBALS['TL_LANG']['FMD']['download_filter'] = ['Download-Filter'];
$GLOBALS['TL_LANG']['FMD']['download_list'] = ['Download-Liste'];
$GLOBALS['TL_LANG']['FMD']['download_detail'] = ['Download-Details'];
