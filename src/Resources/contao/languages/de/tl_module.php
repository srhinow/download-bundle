<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension download-bundle.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license LGPL 3.0 or later
 */

$GLOBALS['TL_LANG']['tl_module']['download_legend'] = 'Download - Einstellungen';
$GLOBALS['TL_LANG']['tl_module']['filter_legend'] = 'Filter - Einstellungen';

/*
 * Fields
 */
$GLOBALS['TL_LANG']['tl_module']['download_template'] = [
    'Download-Template',
    'Wird zur darstellung des einzelnen Elements benötigt.',
];
$GLOBALS['TL_LANG']['tl_module']['do_category_filter'] = [
    'nach Kategorien filtern',
    'Soll es die Möglichkeit geben nur Download bestimmter Kategorien anzuzeigen statt alle dann 
    aktivieren sie die Checkbox.',
];
$GLOBALS['TL_LANG']['tl_module']['download_categories'] = [
    'Kategorien',
    'Nur Downloads dieser Kategorie/n werden angezeigt.',
];
$GLOBALS['TL_LANG']['tl_module']['only_subcategories'] = [
    'nur Unter-Kategorien anzeigen',
    'Nur Downloads Unter-Kategorie/n werden von den ausgewählten Kategorien angezeigt.',
];

$GLOBALS['TL_LANG']['tl_module']['download_size_decimal'] = [
    'Dezimalstellen bei Größenangabe',
    'bei 1 z.B. 13,6 MB',
];

