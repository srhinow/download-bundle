<?php

declare(strict_types=1);

/**
 * Created by fensterart.de c413.
 * Developer: Sven Rhinow (sven@sr-tag.de)
 * Date: 23.01.24
 */

namespace Srhinow\DownloadBundle\Csrf;

use Contao\CoreBundle\Csrf\ContaoCsrfTokenManager;

class DownloadCsrfTokenManager
{
    private ContaoCsrfTokenManager $csrfTokenManager;
    private string $csrfTokenName;

    public function __construct(
        ContaoCsrfTokenManager $csrfTokenManager,
        string $csrfTokenName
    ) {
        $this->csrfTokenName = $csrfTokenName;
        $this->csrfTokenManager = $csrfTokenManager;
    }

    public function generateToken(): string
    {
        return $this->csrfTokenManager->getToken($this->csrfTokenName)->getValue();
    }
}
