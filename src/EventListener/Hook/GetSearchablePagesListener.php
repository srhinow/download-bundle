<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension download-bundle.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license LGPL 3.0 or later
 */

namespace Srhinow\DownloadBundle\EventListener\Hook;

use Contao\Config;
use Contao\Environment;
use Contao\ModuleModel;
use Contao\PageModel;
use Contao\System;
use Srhinow\DownloadBundle\Models\DownloadsModel;

class GetSearchablePagesListener
{
    /**
     * fügt die Download-Detailseiten dem Suchindex hinzu.
     */
    public function onGetSearchablePages(array $pages): array
    {

        if (null === ($objDownloadDetailModul = ModuleModel::findOneByType('downloadDetail'))) {
            return $pages;
        }

        if (null === ($objDownloadListModul = ModuleModel::findOneByType('downloadList'))) {
            return $pages;
        }

        $itemsObj = DownloadsModel::findBy(['published=?'], ['1']);
        if (null === $itemsObj) {
            return $pages;
        }

        $objParent = PageModel::findWithDetails($objDownloadListModul->jumpTo);

        // Set the domain (see #6421)
        $domain = ($objParent->rootUseSSL ? 'https://' : 'http://')
            .($objParent->domain ?: Environment::get('host')).TL_PATH;

        $objUrlGenerator = System::getContainer()->get('contao.routing.url_generator');

        while ($itemsObj->next()) {
            // Link to the default page
            $pages[] = $domain.$objUrlGenerator->generate(
                ($objParent->alias ?: $objParent->id)
                .((Config::get('useAutoItem') && !Config::get('disableAlias'))
                    ? '/'.$itemsObj->alias
                    : '/items/'.$itemsObj->id),
                [
                    '_locale' => $objParent->rootLanguage,
                    '_domain' => $objParent->domain,
                    '_ssl' => (bool) $objParent->rootUseSSL,
                ]
            );
        }

        return $pages;
    }
}
