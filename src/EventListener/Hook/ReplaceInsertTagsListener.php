<?php

declare(strict_types=1);

/**
 * Created by fensterart.de c413.
 * Developer: Sven Rhinow (sven@sr-tag.de)
 * Date: 25.03.24
 */

namespace Srhinow\DownloadBundle\EventListener\Hook;

use Contao\CoreBundle\Framework\ContaoFramework;
use Contao\Input;
use Contao\StringUtil;

class ReplaceInsertTagsListener
{
    public function __construct(
        private ContaoFramework $framework,
    )
    {}

    public function onInsertTagsListener(
        string $insertTag,
        bool $useCache,
        string $cachedValue,
        array $flags,
        array $tags,
        array $cache,
        int $_rit,
        int $_cnt
    )
    {

        if ('download::' === substr($insertTag, 0, 10)) {
            return $this->replaceDownloadInsertTags($insertTag);
        }

        return false;
    }

    public function replaceDownloadInsertTags($insertTag): bool|string
    {
        if ('download::' !== substr($insertTag, 0, 10)) {
            return false;
        }

        //inserttag in Stuecke teilen
        $split = explode('::', $insertTag);

        switch($split[1]) {
            case 'search':
                if(Input::post('search')) {

                    $strSearch = StringUtil::specialchars(Input::post('search'));
                    $strSearch = str_replace([';','/',':','<','>'],'',$strSearch);
                    return strip_tags($strSearch);
                }
                break;
            case 'tag':
                if(Input::post('tag')) {

                    $strSearch = StringUtil::specialchars(Input::post('tag'));
                    $strSearch = str_replace([';','/',':','<','>'],'',$strSearch);
                    return strip_tags($strSearch);
                }
                break;
        }

        return false;
    }
}
