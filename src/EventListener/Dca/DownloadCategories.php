<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension download-bundle.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license LGPL 3.0 or later
 */

namespace Srhinow\DownloadBundle\EventListener\Dca;

use Contao\Backend;
use Contao\DataContainer;
use Contao\StringUtil;
use Srhinow\DownloadBundle\Models\DownloadCategoriesModel;

class DownloadCategories extends Backend
{
    /**
     * Import the back end user object.
     */
    public function __construct()
    {
        parent::__construct();
        $this->import('BackendUser', 'User');
    }

    /**
     * Auto-generate an alias if it has not been set yet.
     *
     * @param mixed
     * @param DataContainer
     *
     * @return string
     */
    public function generateAlias($varValue, DataContainer $dc)
    {
        $autoAlias = false;

        // Generate an alias if there is none
        if ('' === $varValue) {
            $autoAlias = true;
            $varValue = standardize(StringUtil::restoreBasicEntities($dc->activeRecord->name));
        }

        $countAlias = DownloadCategoriesModel::countBy(['alias=?'], $varValue);

        // Check whether the page alias exists
        if ($countAlias > 1) {
            if (!$autoAlias) {
                throw new \Exception(sprintf($GLOBALS['TL_LANG']['ERR']['aliasExists'], $varValue));
            }

            $varValue .= '-'.$dc->id;
        }

        return $varValue;
    }
}
