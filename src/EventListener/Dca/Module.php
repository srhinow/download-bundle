<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension download-bundle.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license LGPL 3.0 or later
 */

namespace Srhinow\DownloadBundle\EventListener\Dca;

use Contao\Backend;
use Contao\Controller;
use Contao\DataContainer;
use Contao\System;
use Contao\TagModel;
use Srhinow\DownloadBundle\Helper\CategoryHelper;
use Srhinow\DownloadBundle\Models\DownloadCategoriesModel;

class Module extends Backend
{
    /**
     * Import the back end user object.
     */
    public function __construct()
    {
        parent::__construct();
        $this->import('BackendUser', 'User');
    }

    /**
     * Return all news templates as array.
     *
     * @return array
     */
    public function getDownloadTemplates()
    {
        return $this->getTemplateGroup('download_');
    }

    public function getCategoryOptions(DataContainer $dc): array
    {
        /** @var CategoryHelper $categoryHelper */
        $categoryHelper = System::getContainer()->get('srhinow.download_bundle.helper.category_helper');
        return $categoryHelper->getDownloadCategoriesAsSelectOptions($dc, true);
    }

   public function getTagOptions(DataContainer $dc): array
    {
        if (null === ($objTags = TagModel::findBy('from_table','tl_downloads'))) {
            return [];
        }

        $arrTags = [];
        while($objTags->next()) {
            if(in_array($objTags->tag, $arrTags)) {
                continue;
            }

            $arrTags[] = $objTags->tag;
        }

        return $arrTags;
    }
}
