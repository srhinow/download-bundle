<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension download-bundle.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license LGPL 3.0 or later
 */

namespace Srhinow\DownloadBundle\EventListener\Dca;

use Contao\Backend;
use Contao\Controller;
use Contao\CoreBundle\Image\ImageFactory;
use Contao\CoreBundle\Image\PictureFactory;
use Contao\DataContainer;
use Contao\FilesModel;
use Contao\Image;
use Contao\Input;
use Contao\StringUtil;
use Contao\System;
use Contao\TagModel;
use Contao\Versions;
use Srhinow\DownloadBundle\Helper\CategoryHelper;
use Srhinow\DownloadBundle\Models\DownloadCategoriesModel;
use Srhinow\DownloadBundle\Models\DownloadsModel;

class Downloads extends Backend
{
    /**
     * Import the back end user object.
     */
    public function __construct(
        private PictureFactory $pictureFactory,
        private string $projectDir
    )
    {
        parent::__construct();
        $this->import('BackendUser', 'User');
    }

    /**
     * Auto-generate an alias if it has not been set yet.
     *
     * @param mixed
     * @param DataContainer
     *
     * @return string
     */
    public function generateAlias($varValue, DataContainer $dc)
    {
        $autoAlias = false;

        // Generate an alias if there is none
        if ('' === $varValue) {
            $autoAlias = true;
            $varValue = standardize(StringUtil::restoreBasicEntities($dc->activeRecord->name));
        }

        $countAlias = DownloadsModel::countBy(['alias=?'], [$varValue]);

        // Check whether the page alias exists
        if ($countAlias > 1) {
            if (!$autoAlias) {
                throw new \Exception(sprintf($GLOBALS['TL_LANG']['ERR']['aliasExists'], $varValue));
            }
            $varValue .= '-'.$dc->id;
        }

        return $varValue;
    }

    /**
     * modify list-view.
     *
     * @param array
     * @param string
     *
     * @return string
     */
    public function listEntries($row, $label, DataContainer $dc, $args)
    {
        $imgLink = true;
        $linkContent = $row['name'];

        if ('' !== $row['file']) {
            if (null !== ($objFile = FilesModel::findByUuid($row['file']))) {
                $linkContent = $objFile->name;
            } else {
                $imgLink = false;
            }
        }

        if ('' !== $row['singleSRC']) {
            if (null !== ($objImage = FilesModel::findByUuid($row['singleSRC']))) {
                $pictureFactory =  $this->pictureFactory->create(
                    $objImage->getAbsolutePath(),
                    [100, 100, Image\ResizeConfiguration::MODE_BOX]
                );

                /** @var Image\DeferredImage $arrPictureData */
                $arrPictureData = $pictureFactory->getImg()['src'];
                $linkContent = sprintf('<img src="%s">',$arrPictureData->getUrl($this->projectDir));
            }
        }

        if($imgLink) {
            $args[0] = sprintf(
                '<a href="%s" target="_blank" title="Datei %s öffnen">%s</a>',
                $objFile->path,
                $row['name'],
                $linkContent
                );
        } else {
            $args[0] = $linkContent;
        }
        $args[2] = isset($row['category'])?$this->getCategoriesForRowArgs($row['category']):'';
        $args[3] = $this->getTagsAtributeForRowArgs($row);

        return $args;
    }

    protected function getCategoriesForRowArgs(?string $strCategory = ''): string
    {
        if (!strlen($strCategory)) {
            return '';
        }

        $arrCategoryNames = [];

        try {
            $arrCategoryId = StringUtil::deserialize($strCategory);
            if(!is_array($arrCategoryId) || count($arrCategoryId) < 1) {
                return '';
            }

            if(null === ($objCategories = DownloadCategoriesModel::findMultipleByIds($arrCategoryId))) {
                return '';
            }

            while($objCategories->next()) {
                $arrCategoryNames[] = $objCategories->name;
            }

        } catch(\Exception $e) {
            return '';
        }

        return implode(', ',$arrCategoryNames);
    }

    protected function getTagsAtributeForRowArgs(array $row): string
    {
        if (!class_exists('Contao\TagModel')) {
            return '';
        }

        if(null === ($objTags = TagModel::findByIdAndTable($row['id'],'tl_downloads'))) {
            return '';
        }

        $arrTags = [];
        foreach($objTags->next() as $objTag) {
            $arrTags[] = $objTag->tag;
        }

        return implode(', ',$arrTags);
    }

    /**
     * Return the "toggle visibility" button.
     *
     * @param array  $row
     * @param string $href
     * @param string $label
     * @param string $title
     * @param string $icon
     * @param string $attributes
     *
     * @return string
     */
    public function toggleIcon($row, $href, $label, $title, $icon, $attributes)
    {
        if (null !== Input::get('tid')) {
            $this->toggleVisibility(
                Input::get('tid'),
                (1 === (int) Input::get('state')),
                (@func_get_arg(12) ?: null)
            );
            $this->redirect($this->getReferer());
        }

        // Check permissions AFTER checking the tid, so hacking attempts are logged
        if (!$this->User->hasAccess('tl_downloads::published', 'alexf')) {
            return '';
        }

        $href .= '&amp;tid='.$row['id'].'&amp;state='.($row['published'] ? '' : 1);

        if (!$row['published']) {
            $icon = 'invisible.gif';
        }

        return '<a href="'.$this->addToUrl($href).'" title="'.StringUtil::specialchars($title).'"'.$attributes.'>'
            .Image::getHtml($icon, $label, 'data-state="'.($row['published'] ? 1 : 0).'"')
            .'</a> ';
    }

    /**
     * Disable/enable a user group.
     *
     * @param int           $intId
     * @param bool          $blnVisible
     * @param DataContainer $dc
     */
    public function toggleVisibility($intId, $blnVisible, DataContainer $dc = null): void
    {
        // Set the ID and action
        Input::setGet('id', $intId);
        Input::setGet('act', 'toggle');

        if ($dc) {
            $dc->id = $intId; // see #8043
        }

        $objVersions = new Versions('tl_downloads', $intId);
        $objVersions->initialize();

        // Trigger the save_callback
        if (\is_array($GLOBALS['TL_DCA']['tl_downloads']['fields']['published']['save_callback'])) {
            foreach ($GLOBALS['TL_DCA']['tl_downloads']['fields']['published']['save_callback'] as $callback) {
                if (\is_array($callback)) {
                    $this->import($callback[0]);
                    $blnVisible = $this->{$callback[0]}->{$callback[1]}($blnVisible, ($dc ?: $this));
                } elseif (\is_callable($callback)) {
                    $blnVisible = $callback($blnVisible, ($dc ?: $this));
                }
            }
        }

        // Update the database
        $objDownload = DownloadsModel::findByPk($intId);
        $objDownload->tstamp = time();
        $objDownload->published = ($blnVisible ? '1' : '');
        $objDownload->save();

        $objVersions->create();
    }

    public function getDownloadCategoryOptions(DataContainer $dc): array
    {
        /** @var CategoryHelper $categoryHelper */
        $categoryHelper = System::getContainer()->get('srhinow.download_bundle.helper.category_helper');
        return $categoryHelper->getDownloadCategoriesAsSelectOptions($dc, false);
    }
}
