<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension download-bundle.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license LGPL 3.0 or later
 */

namespace Srhinow\DownloadBundle\Models;

use Contao\Model;

class DownloadsModel extends Model
{
    /**
     * Table name.
     *
     * @var string
     */
    protected static $strTable = 'tl_downloads';

    /**
     * Find published downloads items by their category ID.
     *
     * @param int   $catid      ID from download category
     * @param int   $intLimit   An optional limit
     * @param int   $intOffset  An optional offset
     * @param array $arrOptions An optional options array
     *
     * @return Model|Model[]|Model\Collection|DownloadsModel|null
     */
    public static function findPublishedByCategory(
        int $catid = 0,
        string $search = '',
        int $intLimit = 0,
        int $intOffset = 0,
        array $arrOptions = []
    ){
        if ($catid < 1) {
            return null;
        }

        $t = static::$strTable;
        $arrColumns = [
            "$t.category LIKE '%\"".$catid."\"%'",
            "$t.published = 1"
        ];

        if (strlen($search) > 0) {
            $arrColumns[] = "(UPPER(name) like '%".strtoupper($search)."%' OR UPPER(text) like '%".strtoupper($search)."%')";
        }


        if (!isset($arrOptions['order'])) {
            $arrOptions['order'] = "$t.sorting ASC";
        }

        return static::findBy($arrColumns, null, $arrOptions);
    }

    /**
     * Find all downloads by given category-id.
     *
     * @return Model|Model[]|Model\Collection|DownloadsModel|null
     */
    public static function findPublishedByCategories($arrCats, array $arrOptions = [])
    {
        $t = static::$strTable;

        if (\is_array($arrCats) && \count($arrCats) > 0 && null !== $arrCats[0]) {
            $catsWhere = [];
            foreach ($arrCats as $catId) {
                $catsWhere[] = "$t.category LIKE '%\"".$catId."\"%'";
            }
            $arrColumns[] = '('.implode(' OR ', $catsWhere).')';
        }
        $arrColumns[] = "$t.published = '1'";

        $arrValues = [];
        $arrColumns = [implode(' AND ', $arrColumns)];

        if (!isset($arrOptions['order'])) {
            $arrOptions['order'] = "$t.sorting DESC";
        }

        return static::findBy($arrColumns, $arrValues, $arrOptions);
    }

    /**
     * Count published downloads items by their category ID.
     *
     * @param int   $catid      ID from download category
     * @param array $arrOptions An optional options array
     *
     * @return int The number of downloads items
     */
    public static function countPublishedByCategory(int $catid = 0, string $search = '', array $arrOptions = [])
    {
        if ($catid < 1) {
            return 0;
        }

        $t = static::$strTable;
        $arrColumns = [
            "$t.category LIKE '%\"".$catid."\"%'",
            "$t.published = 1"
        ];

        if (strlen($search) > 0) {
            $arrColumns[] = "(UPPER(name) like '%".strtoupper($search)."%' OR UPPER(text) like '%".strtoupper($search)."%')";
        }

        return static::countBy($arrColumns, null, $arrOptions);
    }

    /**
     * Find all downloads by given category-id.
     *
     * @return Model|Model[]|Model\Collection|DownloadsModel|null
     */
    public static function findPublishedBySearch(string $search = '', array $arrOptions = [])
    {
        $t = static::$strTable;

        $arrColumns[] = "$t.published = '1'";
        $arrValues = [];

        if(strlen($search) < 1) {
            return null;
        }

        $arrWords = explode(' ', $search);

        if (count($arrWords) > 0) {
            foreach($arrWords as $word) {
                $arrSearches[] = "(LOWER(name) like '%".strtolower($word)."%' OR LOWER(text) like '%".strtolower($word)."%')";
            }
            $arrColumns[] = implode(' OR ', $arrSearches);
        }

        $arrColumns = [implode(' AND ', $arrColumns)];

        if (!isset($arrOptions['order'])) {
            $arrOptions['order'] = "$t.sorting DESC";
        }

        return static::findBy($arrColumns, $arrValues, $arrOptions);
    }
}
