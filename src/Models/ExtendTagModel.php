<?php

declare(strict_types=1);

/**
 * Created by fensterart.de c413.
 * Developer: Sven Rhinow (sven@sr-tag.de)
 * Date: 29.01.24
 */

namespace Srhinow\DownloadBundle\Models;

use Contao\Database;
use Contao\Model\Registry;
use Contao\TagModel;
use Model\Collection;

class ExtendTagModel extends TagModel
{
    /**
     * Find multiple tag results by their IDs and source tables
     *
     * @param int $id ID of the element
     * @param string $table Table name of the element
     *
     * @return \Model\Collection|null A collection of models or null if there are no calendars
     */
    public static function findMultipleByIdsAndTable($arrId, $table)
    {
        if (empty($arrIds) || !\is_array($arrIds))
        {
            return null;
        }

        $arrRegistered = array();
        $arrUnregistered = array();

        // Search for registered models
        foreach ($arrIds as $intId)
        {
            if (empty($arrOptions))
            {
                $arrRegistered[$intId] = Registry::getInstance()->fetch(static::$strTable, $intId);
            }

            if (!isset($arrRegistered[$intId]))
            {
                $arrUnregistered[] = $intId;
            }
        }

        // Fetch only the missing models from the database
        if (!empty($arrUnregistered))
        {
            $t = static::$strTable;

            $arrOptions = array_merge
            (
                array
                (
                    'column' => [
                        "$t.tid IN(" . implode(',', array_map('\intval', $arrIds)) . ")",
                        "$t.from_table=?"
                    ],
                    'value' => [null, $table],
                    'order'  => Database::getInstance()->findInSet("$t.id", $arrIds),
                    'return' => 'Collection'
                ),
                $arrOptions
            );
            $objMissing = static::find($arrOptions);

            if ($objMissing !== null)
            {
                foreach ($objMissing as $objCurrent)
                {
                    $intId = $objCurrent->{static::$strPk};
                    $arrRegistered[$intId] = $objCurrent;
                }
            }
        }

        $arrRegistered = array_filter(array_values($arrRegistered));

        if (empty($arrRegistered))
        {
            return null;
        }

        return static::createCollection($arrRegistered, $t);
        return static::findBy(array('tid=?','from_table=?'), array($id, $table), array('order'=>'tag ASC'));
    }

    public static function findMultipleByTagNamesAndTable(array $arrTags = [], string $table='', array $arrOptions = []): ?Collection
    {
        $t = static::$strTable;

        $arrOptions = array_merge
        (
            array
            (
                'column' => [
                    "$t.tag IN(\"" . implode('","', array_map('\strval',$arrTags)) . "\")",
                    "$t.from_table=?"
                ],
                'value' => [$table],
                'return' => 'Collection'
            ),
            $arrOptions
        );
        return static::find($arrOptions);
    }
}
