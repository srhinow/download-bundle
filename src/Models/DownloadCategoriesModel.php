<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension download-bundle.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license LGPL 3.0 or later
 */

namespace Srhinow\DownloadBundle\Models;

use Contao\Database;
use Contao\Model;
use Contao\Model\Registry;
use Model\Collection;

class DownloadCategoriesModel extends Model
{
    /**
     * Table name.
     *
     * @var string
     */
    protected static $strTable = 'tl_download_categories';

    public static function findOnlySubCategoriesByParentIds(array $arrIds, array $arrOptions=[]): ?Collection
    {
        if (empty($arrIds) || !\is_array($arrIds))
        {
            return null;
        }

        $arrRegistered = array();
        $arrUnregistered = array();

        // Search for registered models
        foreach ($arrIds as $intId)
        {
            if (empty($arrOptions))
            {
                $arrRegistered[$intId] = Registry::getInstance()->fetch(static::$strTable, $intId);
            }

            if (!isset($arrRegistered[$intId]))
            {
                $arrUnregistered[] = $intId;
            }
        }

        // Fetch only the missing models from the database
        if (!empty($arrUnregistered))
        {
            $t = static::$strTable;

            $arrOptions = array_merge
            (
                array
                (
                    'column' => array("$t.pid IN(" . implode(',', array_map('\intval', $arrIds)) . ")"),
                    'order'  => Database::getInstance()->findInSet("$t.id", $arrIds),
                    'return' => 'Collection'
                ),
                $arrOptions
            );
            $objMissing = static::find($arrOptions);

            if ($objMissing !== null)
            {
                foreach ($objMissing as $objCurrent)
                {
                    $intId = $objCurrent->{static::$strPk};
                    $arrRegistered[$intId] = $objCurrent;
                }
            }
        }

        $arrRegistered = array_filter(array_values($arrRegistered));

        if (empty($arrRegistered))
        {
            return null;
        }

        return static::createCollection($arrRegistered, $t);
    }

    public static function getRootCategoriesByCategoryIds(array $catIds = [], array $arrOptions=[]): array
    {
        if(count($catIds) < 1) {
            return [];
        }

        $arrRootIds = [];

        foreach ($catIds as $id) {
            if(!$rootId = self::getRootCategoryById($id)) {
                continue;
            }
            $arrRootIds[] = $rootId;
        }

        return  $arrRootIds;
    }

    public static function getRootCategoryById($id): false|int
    {
        if (null === ($objCat = DownloadCategoriesModel::findByPk($id))) {
            return false;
        }

        if((int) $objCat->pid > 0) {
            self::getRootCategoryById($objCat->pid);
        }

        return $objCat->id;
    }

    public static function getSubCategoriesByCategoryIds(array $catIds = [], array $arrOptions=[]): array
    {
        if(count($catIds) < 1) {
            return [];
        }

        $arrRootIds = [];

        foreach ($catIds as $id) {
            if(!$rootId = self::getSubCategoryById($id)) {
                continue;
            }
            $arrRootIds[] = $rootId;
        }

        return  $arrRootIds;
    }

    public static function getSubCategoryById($id): false|int
    {
        if (null === ($objCat = DownloadCategoriesModel::findByPk($id))) {
            return false;
        }

        if((int) $objCat->pid > 0) {
            self::getSubCategoryById($objCat->pid);
        }

        return $objCat->id;
    }
}
