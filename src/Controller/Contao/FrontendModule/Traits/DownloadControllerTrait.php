<?php
/**
 * Created by fensterart.de c413.
 * Developer: Sven Rhinow (sven@sr-tag.de)
 * Date: 24.01.24
 */

namespace Srhinow\DownloadBundle\Controller\Contao\FrontendModule\Traits;

use Contao\Config;
use Contao\Controller;
use Contao\Environment;
use Contao\File;
use Contao\FilesModel;
use Contao\FrontendTemplate;
use Contao\Image;
use Contao\Input;
use Contao\ModuleModel;
use Contao\PageModel;
use Contao\StringUtil;
use Contao\System;
use Contao\TagModel;
use Contao\Validator;
use Model\Collection;
use Srhinow\DownloadBundle\Models\DownloadsModel;
use Symfony\Component\HttpFoundation\Request;

trait DownloadControllerTrait
{
    /**
     * URL cache array.
     *
     * @var array
     */
    private static $arrUrlCache = [];

    /**
     * Parse an item and return it as string.
     *
     * @param string $strClass
     *
     * @return string
     */
    protected function parseDownload(DownloadsModel $objDownload, ModuleModel $model, string $strClass = '')
    {
        /* @var PageModel $objPage */
        global $objPage;

        $objFile = FilesModel::findByUuid($objDownload->file);

        if (null === $objFile) {
            if (!Validator::isUuid($objDownload->file)) {
                return '<p class="error">'.$GLOBALS['TL_LANG']['ERR']['version2format'].'</p>';
            }

            return '';
        }

        try {
            $objFile = new File($objFile->path);
        } catch (\Exception $e) {
        }

        if ('' === $model->linkTitle) {
            $model->linkTitle = StringUtil::specialchars($objFile->basename);
        }

        $strFileHref = Environment::get('request');

        // Remove an existing file parameter (see #5683)
        if (preg_match('/(&(amp;)?|\?)file=/', $strFileHref)) {
            $strFileHref = preg_replace('/(&(amp;)?|\?)file=[^&]+/', '', $strFileHref);
        }

        $strFileHref .= (
            (Config::get('disableAlias') || false !== strpos($strFileHref, '?'))
                ? '&amp;'
                : '?'
            ).'file='.\System::urlEncode($objFile->value);

        if (null !== ($objDetailPage = PageModel::findByPk($model->jumpTo))) {
            $objUrlGenerator = System::getContainer()->get('contao.routing.url_generator');

            $strUrl = $objUrlGenerator->generate(
                ($objDetailPage->alias ?: $model->id).'/'.$objDownload->alias,
                [
                    '_locale' => $model->rootLanguage,
                    '_domain' => $model->domain,
                    '_ssl' => (bool) $model->rootUseSSL,
                ]
            );
            $strDetailHref = ampersand($strUrl);
        }

        /** @var \FrontendTemplate|object $objTemplate */
        $objTemplate = new FrontendTemplate($model->download_template);
        $objTemplate->setData($objDownload->row());
        $objTemplate->class = (('' !== $objDownload->cssClass) ? ' '.$objDownload->cssClass : '').$strClass;
        $objTemplate->detail_href = $strDetailHref??'';
        $objTemplate->file_href = $strFileHref;
        $objTemplate->file_name = $objFile->name;
        $objTemplate->filesize = $this->getReadableSize($objFile->filesize, $model->download_size_decimal);
        $objTemplate->icon = Image::getPath($objFile->icon);
        $objTemplate->mime = $objFile->mime;
        $objTemplate->extension = $objFile->extension;
        $objTemplate->path = $objFile->dirname;

        //wenn das hschottm/tags installiert ist auch die Tags einfuegen
        if (null !== $objDownload->tags) {
            $tagLinks = [];
            $tags = [];
            $objTags = TagModel::findByIdAndTable($objDownload->id, 'tl_downloads');

            if (null !== $objTags) {
                while ($objTags->next()) {
                    $tags[] = $objTags->tag;
                }
            }
            $objTemplate->tags = $tags;
        }

        // HOOK: add custom logic
        if (isset($GLOBALS['TL_HOOKS']['parseDownload']) && \is_array($GLOBALS['TL_HOOKS']['parseDownload'])) {
            foreach ($GLOBALS['TL_HOOKS']['parseDownload'] as $callback) {
                $this->import($callback[0]);
                $this->{$callback[0]}->{$callback[1]}($objTemplate, $objDownload->row(), $this);
            }
        }

        return $objTemplate->parse();
    }

    /**
     * Parse one or more items and return them as array.
     *
     * @param \Model\Collection $objDownloads
     *
     * @return array
     */
    protected function parseDownloads($objDownloads, ModuleModel $model)
    {
        $objDownloads->reset();
        $limit = $objDownloads->count();

        if ($limit < 1) {
            return [];
        }

        $count = 0;
        $arrArticles = [];

        while ($objDownloads->next()) {
            /** @var DownloadsModel $objDownload */
            $objDownload = $objDownloads->current();

            $arrArticles[] = $this->parseDownload(
                $objDownload,
                $model,
                ((1 === ++$count) ? ' first' : '').(($count === $limit) ? ' last' : '')
                .((0 === ($count % 2)) ? ' odd' : ' even')
            );
        }

        return $arrArticles;
    }

    public function sendFileToBrowser(Request $request)
    {
        $file = $request->get('file');

        // Send the file to the browser and do not send a 404 header (see #4632)
        if (null !== $file) {
            Controller::sendFileToBrowser($file);
        }
    }

    /**
     * Copy from Contao that this work with AJAX
     *
     * @param integer $intSize     The size in bytes
     * @param integer $intDecimals The number of decimals to show
     *
     * @return string The human-readable size
     */
    public function getReadableSize($intSize, $intDecimals=1)
    {
        $arrUnits = [
            0 => "B",
            1 => "kB",
            2 => "MB",
            3 => "GB",
            4 => "TB",
            5 => "PB",
            6 => "EB",
            7 => "ZB",
            8 => "YB"
        ];
        $decimalSeparator = ",";
        $thousandsSeparator = ".";

        for ($i=0; $intSize>=1024; $i++)
        {
            $intSize /= 1024;
        }

        return number_format(
            round($intSize, $intDecimals),
            $intDecimals,
            $decimalSeparator,
            $thousandsSeparator
            ) . ' ' . $arrUnits[$i];
    }

    protected function isImageEntries(Collection $objDownloads): bool
    {
        $isImage = false;
        $objDownloads->reset();

        while ($objDownloads->next()) {
            if (1 === (int) $objDownloads->addImage && null !== $objDownloads->singleSRC) {
                return true;
            }
        }

        return $isImage;
    }
}
