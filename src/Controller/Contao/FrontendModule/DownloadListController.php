<?php

declare(strict_types=1);

namespace Srhinow\DownloadBundle\Controller\Contao\FrontendModule;

use Contao\Controller;
use Contao\CoreBundle\Controller\FrontendModule\AbstractFrontendModuleController;
use Contao\CoreBundle\ServiceAnnotation\FrontendModule;
use Contao\Input;
use Contao\Model;
use Contao\ModuleModel;
use Contao\PageModel;
use Contao\Template;
use Model\Collection;
use Psr\EventDispatcher\EventDispatcherInterface;
use Srhinow\DownloadBundle\Controller\Contao\FrontendModule\Traits\DownloadControllerTrait;
use Srhinow\DownloadBundle\Helper\TagHelper;
use Srhinow\DownloadBundle\Models\DownloadCategoriesModel;
use Srhinow\DownloadBundle\Models\DownloadsModel;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\RouterInterface;

/**
 * @FrontendModule("download_list",
 *   category="downloads",
 *   template="mod_download_list",
 *   renderer="forward"
 * )
 */
class DownloadListController extends AbstractFrontendModuleController
{
    use DownloadControllerTrait;

    protected $objPage = null;

    public function __construct(
        protected RouterInterface $router,
        protected EventDispatcherInterface $eventDispatcher
    ) {
    }

    protected function getResponse(Template $template, ModuleModel $model, Request $request): Response
    {
        $this->sendFileToBrowser($request);

        global $objPage;

        $this->objPage = $objPage;

        if(null === $this->objPage && null !== $request->get('page')) {
            $this->objPage = PageModel::findByPk($request->get('page'));
        }


        $template->categories = $categories = [];
        $template->empty = $GLOBALS['TL_LANG']['MSC']['emptyDownloadList'];
        $template->attributes = 'data-id="'.$model->id.'"';
        $arrTags = \is_string($request->get('tag')) ? [(string) $request->get('tag')] : [];
        $strSearch = is_string($request->get('search'))?$request->get('search'):'';

        $arrCats = [];
        if(strlen((string) $this->objPage->download_categories) > 0) {
            $arrCats = unserialize($this->objPage->download_categories)?:[];
        }

        if (is_array($arrCats) && count($arrCats) > 0) {
            $template->categories = $this->findDownloadsByCustomCategories($model, $arrCats, $arrTags, $strSearch);
        } else {
            $template->categories = $this->findAllDownloadsByAllCategories($model);
        }

        return $template->getResponse();
    }

    /**
     * @param array $catFilter
     * @return array
     */
    public function findDownloadsByCustomCategories(
        Model $model,
        array $catFilter = [],
        array $arrTags = [],
        string $search = ''
    ): array
    {
        $categories = [];

        if (!\is_array($catFilter) || \count($catFilter) < 1) {
            return $categories;
        }

        if($this->objPage->only_subcategories) {
            $objDwlCategories = DownloadCategoriesModel::findOnlySubCategoriesByParentIds($catFilter, ['order' => 'sorting ASC']);
        } else {
            $objDwlCategories = DownloadCategoriesModel::findMultipleByIds($catFilter, ['order' => 'sorting ASC']);
        }


        if (null !== $objDwlCategories) {
            // 2. Kategorie-Ebene
            while ($objDwlCategories->next()) {

                $downloads = [];
                $isImageStyle = false;

                // test 3. Kategorie-Ebene
                if( null !== ($objThirdDwlCategories = DownloadCategoriesModel::findBy('pid',$objDwlCategories->id))) {

                    while ($objThirdDwlCategories->next()) {
                        if( null === ($objDownloads = $this->getDownloadEntriesByCategoryId(
                            (int) $objThirdDwlCategories->id,
                            $search,
                            $arrTags
                        ))) {
                            continue;
                        }

                        $downloads[] = [
                            'entries' => (null !== $objDownloads) ? $this->parseDownloads($objDownloads, $model) : [],
                            'category' =>  $objThirdDwlCategories->row(),
                        ];
                        $thirdLevel = true;
                        $isImageStyle = $this->isImageEntries($objDownloads);
                    }

                } else {
                    if (null === ($objDownloads = $this->getDownloadEntriesByCategoryId(
                        (int) $objDwlCategories->id,
                        $search,
                        $arrTags
                    ))) {
                        continue;
                    }
                    $downloads = (null !== $objDownloads) ? $this->parseDownloads($objDownloads, $model) : [];
                    $thirdLevel = false;
                    $isImageStyle = $this->isImageEntries($objDownloads);
                }

                if(count($downloads) < 1) {
                    continue;
                }

                $categories[] = [
                    'id' => $objDwlCategories->id,
                    'name' => $objDwlCategories->name,
                    'alias' => $objDwlCategories->alias,
                    'isImageStyle' => $isImageStyle,
                    'thirdLevel' => $thirdLevel,
                    'downloads' => $downloads,
                ];
            }
        }

        return $categories;
    }

    protected function getDownloadEntriesByCategoryId(int $categoryId, string $search ='', array $arrTags = []): ?Collection
    {

        if (DownloadsModel::countPublishedByCategory( $categoryId, $search) < 1) {
            return null;
        }
        $objDownloads = DownloadsModel::findPublishedByCategory($categoryId, $search);

        $objDownloads = TagHelper::filterDownloadsByTags(
            $objDownloads,
            $arrTags
        );

        return $objDownloads;
    }

    /**
     * @return array
     */
    protected function findAllDownloadsByAllCategories(ModuleModel $model, array $arrTags = []): array
    {
        $categories = [];
        $dwlCategoriesObj = DownloadCategoriesModel::findAll(['order' => 'sorting ASC']);

        if (null !== $dwlCategoriesObj) {
            while ($dwlCategoriesObj->next()) {
                if (DownloadsModel::countPublishedByCategory((int) $dwlCategoriesObj->id) < 1) {
                    continue;
                }

                $objDownloads = DownloadsModel::findPublishedByCategory((int) $dwlCategoriesObj->id);

                $entries = (null !== $objDownloads) ? $this->parseDownloads($objDownloads, $model) : [];

                $categories[] = [
                    'id' => $dwlCategoriesObj->id,
                    'name' => $dwlCategoriesObj->name,
                    'alias' => $dwlCategoriesObj->alias,
                    'entries' => $entries,
                ];
            }
        }

        return $categories;
    }

}
