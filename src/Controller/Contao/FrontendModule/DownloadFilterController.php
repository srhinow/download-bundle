<?php

declare(strict_types=1);

namespace Srhinow\DownloadBundle\Controller\Contao\FrontendModule;

use Contao\Controller;
use Contao\CoreBundle\Controller\FrontendModule\AbstractFrontendModuleController;
use Contao\CoreBundle\Exception\RedirectResponseException;
use Contao\CoreBundle\ServiceAnnotation\FrontendModule;
use Contao\ModuleModel;
use Contao\PageModel;
use Contao\Template;
use Psr\EventDispatcher\EventDispatcherInterface;
use Srhinow\DownloadBundle\Form\Type\DownloadFilterType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\RouterInterface;

/**
 * @FrontendModule("download_filter",
 *   category="downloads",
 *   template="mod_download_filter",
 *   renderer="forward"
 * )
 */
class DownloadFilterController extends AbstractFrontendModuleController
{

    public function __construct(
        protected RouterInterface $router,
        protected EventDispatcherInterface $eventDispatcher
    ) {}

    protected function getResponse(Template $template, ModuleModel $model, Request $request): Response
    {
        $isAjax = true;

        $createFormOptions = [
            'fmd' => $model,
        ];

        if ((int) $model->jumpTo > 0 && null !== ($objJumpToPage = PageModel::findByPk($model->jumpTo))) {
            $isAjax = false;
            $route = $objJumpToPage->getFrontendUrl();
            $createFormOptions['action'] = $route;
            $createFormOptions['method'] = 'POST';
        }

        $form = $this->createForm(
            DownloadFilterType::class,
            null,
            $createFormOptions
        );

        global $objPage;

        return $this->renderForm('mod_download_filter.html.twig',
            [
            'form' => $form,
            'isAjax' => $isAjax,
            'ajaxRoute' => $this->router->getRouteCollection()->get('srhinow_download_bundle.download_filter')->getPath(),
            'locale' => $request->getLocale(),
            'page' => $objPage->id,
        ]
        );
        return $template->getResponse();
    }

}
