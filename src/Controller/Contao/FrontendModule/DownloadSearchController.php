<?php

declare(strict_types=1);


namespace Srhinow\DownloadBundle\Controller\Contao\FrontendModule;

use Contao\CoreBundle\Controller\FrontendModule\AbstractFrontendModuleController;
use Contao\CoreBundle\ServiceAnnotation\FrontendModule;
use Contao\ModuleModel;
use Contao\PageModel;
use Contao\TagModel;
use Contao\Template;
use Model\Collection;
use Psr\EventDispatcher\EventDispatcherInterface;
use Srhinow\DownloadBundle\Controller\Contao\FrontendModule\Traits\DownloadControllerTrait;
use Srhinow\DownloadBundle\Helper\TagHelper;
use Srhinow\DownloadBundle\Models\DownloadCategoriesModel;
use Srhinow\DownloadBundle\Models\DownloadsModel;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\RouterInterface;

/**
 * @FrontendModule("download_search",
 *   category="downloads",
 *   template="mod_download_search",
 *   renderer="forward"
 * )
 */
class DownloadSearchController extends AbstractFrontendModuleController
{
    use DownloadControllerTrait;

    protected $objPage = null;

    public function __construct(
        protected RouterInterface $router,
        protected EventDispatcherInterface $eventDispatcher
    ) {
    }

    protected function getResponse(Template $template, ModuleModel $model, Request $request): Response
    {
        $this->sendFileToBrowser($request);

        $arrAllCategories = [];
        $arrDownloadsByCategories = [];

        $template->empty = $GLOBALS['TL_LANG']['MSC']['noEntriesBySearch'];
        $template->attributes = 'data-id="'.$model->id.'"';
        $template->entries = $arrDownloadsByCategories;
        $strTag = \is_string($request->get('tag')) ? (string) $request->get('tag') : '';
        $strSearch = \is_string($request->get('search'))?$request->get('search'):'';

        if (strlen($strTag) < 1 && strlen($strSearch) < 1) {
            $template->empty = $GLOBALS['TL_LANG']['MSC']['noSearchAndNoTags'];
            return $template->getResponse();

        } elseif (strlen($strTag) > 0  && strlen($strSearch) < 1) {

            if (null === ($objTags = TagModel::findBy(['tag=?','from_table=?'],[$strTag, 'tl_downloads']))) {
                return $template->getResponse();
            }

            $arrDownloadIds = [];
            while ($objTags->next()) {
                $arrDownloadIds[] = $objTags->tid;
            }

            if(null === ($objDownloads = DownloadsModel::findMultipleByIds($arrDownloadIds))) {
                return $template->getResponse();
            }

        } else {
            if (null === ($objDownloads = DownloadsModel::findPublishedBySearch($strSearch))) {

                return $template->getResponse();
            }

            if(strlen($strTag)>0) {
                $objDownloads = TagHelper::filterDownloadsByTags($objDownloads, [$strTag]);
                $objDownloads->reset();
            }

        }

        while($objDownloads->next()) {
            $arrCategory = unserialize($objDownloads->category);
            $arrAllCategories = array_unique(array_merge($arrAllCategories,$arrCategory));
            $arrDownloadsByCategories = $this->addDownloadToCategories($objDownloads->current(), $model, $arrCategory, $arrDownloadsByCategories);
        }
        $template->entries = $arrDownloadsByCategories;

        return $template->getResponse();
    }

    protected function addDownloadToCategories(
        DownloadsModel $objDownload,
        ModuleModel $model,
        array $arrDownloadCategory = [],
        array $arrDownloadsByCategories = []
        ): array
    {

        foreach($arrDownloadCategory as $catId) {
            if(!isset($arrDownloadsByCategories[$catId])) {
                $arrDownloadsByCategories[$catId]['isImageStyle'] = false;
            }
            if (1 === (int) $objDownload->addImage && null !== $objDownload->singleSRC) {
                $arrDownloadsByCategories[$catId]['isImageStyle'] = true;
            }

            $arrDownloadsByCategories[$catId]['downloads']['data'][] = $objDownload->row();
            $arrDownloadsByCategories[$catId]['downloads']['parsed'][] = $this->parseDownload($objDownload, $model);
            $arrDownloadsByCategories[$catId]['titles'] =$this->getCategoryTitle((int) $catId);
        }

        return $arrDownloadsByCategories;
    }

    protected function getCategoryTitle(int $catId, &$arrCatTitles=[]): array
    {
        if (null === ($objCurrentCategory = DownloadCategoriesModel::findByPk($catId))) {
            return $arrCatTitles;
        }

        $arrCatTitles[] = $objCurrentCategory->name;

        if (0 === (int) $objCurrentCategory->pid) {

            if(count($arrCatTitles) > 1) {
                $arrCatTitles = array_reverse($arrCatTitles);
            }

            return $arrCatTitles;

        } else {

            $this->getCategoryTitle((int) $objCurrentCategory->pid, $arrCatTitles);
        }

        return $arrCatTitles;
    }
}
