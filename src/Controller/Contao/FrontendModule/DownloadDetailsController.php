<?php

declare(strict_types=1);


namespace Srhinow\DownloadBundle\Controller\Contao\FrontendModule;

use Contao\Controller;
use Contao\CoreBundle\Controller\FrontendModule\AbstractFrontendModuleController;
use Contao\CoreBundle\ServiceAnnotation\FrontendModule;
use Contao\Input;
use Contao\ModuleModel;
use Contao\StringUtil;
use Contao\Template;
use Psr\EventDispatcher\EventDispatcherInterface;
use Srhinow\DownloadBundle\Controller\Contao\FrontendModule\Traits\DownloadControllerTrait;
use Srhinow\DownloadBundle\Models\DownloadsModel;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\RouterInterface;

/**
 * @FrontendModule("download_detail",
 *   category="downloads",
 *   template="mod_download_details",
 *   renderer="forward"
 * )
 */
class DownloadDetailsController extends AbstractFrontendModuleController
{
    use DownloadControllerTrait;


    protected function getResponse(Template $template, ModuleModel $model, Request $request): Response
    {
        $this->sendFileToBrowser($request);

        global $objPage;

        // Get the total number of items
        $objDownload = DownloadsModel::findByIdOrAlias($request->get('download'));

        if (null === $objDownload) {
            // Do not index or cache the page
            $objPage->noSearch = 1;
            $objPage->cache = 0;

            // Send a 404 header
            header('HTTP/1.1 404 Not Found');
            $template->articles =
                '<p class="error">'.sprintf($GLOBALS['TL_LANG']['MSC']['invalidPage'], \Input::get('items')).'</p>';

            return $template->getResponse();
        }

        $strDetails = $this->parseDownload($objDownload);
        $template->details = $strDetails;

        // Overwrite the page title (see #2853 and #4955)
        if ('' !== $objDownload->pageTitle) {
            $objPage->pageTitle = strip_tags(StringUtil::stripInsertTags($objDownload->name));
        }

        // Overwrite the page description
        if ('' !== $objDownload->description) {
            $objPage->description = $this->prepareMetaDescription($objDownload->description);
        }

        $template->referer = 'javascript:history.go(-1)';
        $template->back = $GLOBALS['TL_LANG']['MSC']['goBack'];

        return $template->getResponse();
    }

}
