<?php

declare(strict_types=1);

/**
 * Created by fensterart.de c413.
 * Developer: Sven Rhinow (sven@sr-tag.de)
 * Date: 22.01.24
 */

namespace Srhinow\DownloadBundle\Controller;

use Contao\Module;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class DownloadFilterRequestController extends AbstractController
{
    public function filterDownloadsAction(Request $request): Response
    {
        $module = Module::getFrontendModule($request->get('id'));
        return new Response($module);
    }

}
