<?php

declare(strict_types=1);

/**
 * Created by fensterart.de c413.
 * Developer: Sven Rhinow (sven@sr-tag.de)
 * Date: 23.01.24
 */

namespace Srhinow\DownloadBundle\Helper;

use Contao\Model;
use Contao\TagModel;
use Model\Collection;
use Srhinow\DownloadBundle\Models\DownloadCategoriesModel;
use Srhinow\DownloadBundle\Models\DownloadsModel;
use Srhinow\DownloadBundle\Models\ExtendTagModel;

class TagHelper
{
    public static function getTagOptions(): array
    {
        global $objPage;

        $arrOptions = [];

        $arrCats = [];
        if (strlen((string) $objPage->download_tags) > 0) {
            $arrTags = unserialize($objPage->download_tags)?:[];
            if (null === ($objTags = ExtendTagModel::findMultipleByTagNamesAndTable($arrTags, DownloadsModel::getTable()))) {
                return $arrOptions;
            }
        } else {
            if (null === ($objTags = ExtendTagModel::findby('from_table',DownloadsModel::getTable()))) {
                return $arrOptions;
            }
        }

        while ($objTags->next()) {
            $arrOptions[$objTags->tag] = $objTags->tag;
        }

        return $arrOptions;
    }

    public static function filterDownloadsByTags(?Collection $objDownloads, array $arrTags = [])
    {
        if(count($arrTags) < 1 || strlen((string) $arrTags[0]) < 1) {
            return $objDownloads;
        }

        $arrDownloads = [];
        while ($objDownloads->next()) {
            if (self::isTagInDownload((int) $objDownloads->id, $arrTags)) {
                $arrDownloads[] = $objDownloads->current();
            }
        }

        return new Collection($arrDownloads, DownloadsModel::getTable());
    }

    public static function isTagInDownload(int $downloadId, array $arrTags = []): bool
    {
        $return = false;

        foreach($arrTags as $tag) {
            if (null !== ($objTag = TagModel::findBy(['tag=?','tid=?','from_table=?'],[$tag, $downloadId, 'tl_downloads']))) {
                $return = true;
            }
        }

        return $return;
    }
}
