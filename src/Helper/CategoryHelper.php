<?php

declare(strict_types=1);

/**
 * Created by fensterart.de c413.
 * Developer: Sven Rhinow (sven@sr-tag.de)
 * Date: 26.01.24
 */

namespace Srhinow\DownloadBundle\Helper;

use Contao\CoreBundle\Framework\ContaoFramework;
use Contao\DataContainer;
use Srhinow\DownloadBundle\Models\DownloadCategoriesModel;

class CategoryHelper
{
    private $strOptionName = '';

    public function __construct(
        private ContaoFramework $framework
    )
    {}


    public function getDownloadCategoriesAsSelectOptions(DataContainer $dc, bool $onlyFirstLevel = true): array
    {
        $arrOptions = [];
        $this->strOptionName = '';

        if (null === ($objFirstLevelCategories = DownloadCategoriesModel::findBy('pid',0))) {
            return $arrOptions;
        }

        while ($objFirstLevelCategories->next()) {
            $arrOptions[$objFirstLevelCategories->id] = $objFirstLevelCategories->name;
            if(!$onlyFirstLevel) {
                if (null === ($objSecondLevelCategories = DownloadCategoriesModel::findBy('pid',$objFirstLevelCategories->id))) {
                    continue;
                }

                while($objSecondLevelCategories->next()) {
                    if (null === ($objThirdLevelCategories = DownloadCategoriesModel::findBy('pid',$objSecondLevelCategories->id))) {
                        $arrOptions[$objSecondLevelCategories->id] = $objFirstLevelCategories->name . ' : '.$objSecondLevelCategories->name;
                    } else {
                        while($objThirdLevelCategories->next()) {
                            $arrOptions[$objThirdLevelCategories->id] = $objFirstLevelCategories->name . ' : '.$objSecondLevelCategories->name .' : '.$objThirdLevelCategories->name;
                        }
                    }



                }

                //                if (null === ($arrData = $this->getRecursiveLevelByCategoryId(
//                    $objFirstLevelCategories->id,
//                    [$objFirstLevelCategories->name]
//                    ))) {
//                    continue;
//                }
//
//                $arrOptions[$arrData['id']] = implode(' : ',$arrData['names']);

//                dd($arrData);
//
            }
        }

        return $arrOptions;
    }

//    protected function getRecursiveLevelByCategoryId(int $CategoryId = 0, array $arrCategoryLevelNames = []): ?array
//    {
//        if(null === ($objCurrentCategory = DownloadCategoriesModel::findByPk($CategoryId))) {
//            return null;
//        }
//
//        if (null === ($objNextLevelCategories = DownloadCategoriesModel::findBy('pid',$CategoryId))) {
//            return null;
//        }
//
//        while ($objNextLevelCategories->next()) {
//            $arrCategoryLevelNames[] = $objNextLevelCategories->name;
//            if (null === $this->getRecursiveLevelByCategoryId($objNextLevelCategories->id, $arrCategoryLevelNames)) {
//                return [
//                    'id' => $objNextLevelCategories->id,
//                    'names' =>$arrCategoryLevelNames,
//                ];
//            }
//        }
//
//    }
}
